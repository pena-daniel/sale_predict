const form = document.querySelector('#form')
const btn = document.querySelector('form button')
const secteur = document.querySelector('#secteur')
const result_p = document.querySelector('.result p')
const result = document.querySelector('.result')
const result_img = document.querySelector('#result_img')
const result_table = document.querySelector('#result_table')
const table_inner = document.querySelector('#result_table table tbody')
const result_inner_p = document.querySelector('.result_image_t p')
const url = "http://127.0.0.1:5000/api/predict"
let isSending = false;

const month_letter = {
    "1": "Janvier",
    "2": "Fevrier",
    "3": "Mars",
    "4": "Avril",
    "5": "Mai",
    "6": "Juin",
    "7": "Juillet",
    "8": "Aout",
    "9": "Septembre",
    "10": "Octobre",
    "11": "Novembre",
    "12": "Decembre",
}



document.addEventListener("DOMContentLoaded", (event) => {
    form.addEventListener('submit', async (e) => {
        e.preventDefault()
        if (isSending) return;

        // isSending
        result_img.classList.add("none")
        result_table.classList.add("none")
        result.classList.add("empty")
        result.classList.remove("error")
        btn.classList.add("disabled")
        btn.innerHTML = "Chargement..."
        isSending = true

        // perform request
        try{
            const response = await fetch(
                url,
                {
                    method: "POST",
                    headers: {
                        "Content-type": "application/json"
                    },
                    body: JSON.stringify({
                        "secteur": secteur.value,
                    })
                }
            )

            const res = await response.json()
            btn.classList.remove("disabled")
            btn.innerHTML = "Predire"
            isSending = false

            if (response.status != 200) {
                result.classList.add("error")
                result_p.innerHTML = res.message
                result.classList.toggle("empty")
                return
            }
            result_img.src = `data:image/jpeg;base64,${res.figure}`
            result_img.classList.remove("none")

            //data
            data = []

            Object.keys(res.x).forEach((key) => {
            let d = res.y[key].split('/')
                data.push({
                    month: d[0],
                    year: d[1],
                    value: res.x[key].toFixed(2),
                })
            })
            const data_group = Object.groupBy(data, ({ month }) => month);

            let years = [2023, 2024, 2025]


            //header
            let tr = ""
            table_inner.innerHTML = ""
            console.log(Object.keys(data_group))
            Object.keys(data_group).sort((a, b) => a - b).forEach((month) => {
                tr = ""
                console.log(month)
                tr += `<td><span>${month_letter[+month]}</span></td>`
                let ys = data_group[month]

                ys.forEach((v, i) => {
                    if(i == 0 && v.year != 2023){
                        tr += `<td><span>-</span></td>`
                    }
                    if(i == ys.length-1 && v.year != 2025){
                        tr += `<td><span>${v.value}</span></td>`
                        tr += `<td><span>-</span></td>`
                    }else{
                        tr += `<td><span>${v.value}</span></td>`
                    }
                })
                table_inner.innerHTML += `<tr>${tr}</tr>`
            })
            //rows
            result_inner_p.innerHTML = "Prediction des revenus sur la periode du 31/08/2023 au 31/08/2025. les salaire ici sont en : "
            result_inner_p.innerHTML += `Millions de Rands`
            result_table.classList.remove("none")

        }
        catch(e){
            console.log("erreur")
            console.log(e)
            btn.classList.remove("disabled")
            btn.innerHTML = "Predire"
            isSending = false
            alert("Une erreur est survenue veuillez reessayer !")
        }
    })
});



