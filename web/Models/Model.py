import pandas as pd
import os
import joblib


def get_sectors():
    path = os.path.abspath('web/static/data/secteur.pkl')
    return joblib.load(path)


def get_french_sectors():
    path = os.path.abspath('web/static/data/secteurs_traduits.pkl')
    return joblib.load(path)


def get_models():
    path = os.path.abspath('web/static/data/model.pkl')
    return joblib.load(path)


sectors = get_sectors()
sectors_french = get_french_sectors()
models = get_models()
