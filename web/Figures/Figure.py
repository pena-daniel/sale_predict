from matplotlib import pyplot as plt
import base64
from io import BytesIO


def get_line_plot(data_y, data_x, title=None) -> str:
    # Generate the figure
    fig = plt.figure(figsize=(10, 6))
    plt.plot(data_y, data_x, color='red')
    plt.xticks(rotation=40, ha="right")
    plt.xlabel('Date')
    plt.ylabel('Entrées mensuelles')

    if title is not None:
        plt.title(f'Entrées mensuelles pour le secteur "{title}"')
    # Save it to a temporary buffer.
    buf = BytesIO()
    fig.savefig(buf, format="png")
    return base64.b64encode(buf.getbuffer()).decode("utf-8")
