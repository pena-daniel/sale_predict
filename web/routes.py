import math

from flask import Flask, render_template, request, url_for, redirect, jsonify
from flask_cors import CORS

from web.Figures.Figure import get_line_plot
from web.Models.Model import models, sectors, sectors_french

app = Flask(__name__)
app.config.from_object('config.Config')
CORS(app, resources={r"/api/*": {"origins": "*"}})


@app.errorhandler(500)
def error(er):
    return render_template('error.html', link="/", code=500, message=er)


@app.errorhandler(404)
def error(er):
    return render_template('error.html', link="/", code=404, message=er)


@app.errorhandler(400)
def error(er):
    return render_template('error.html', link="/", code=400, message=er)


"""
@app.errorhandler(415)
def error(er):
    return render_template('error.html', link="/", code=415, message=er)
"""


@app.route('/', methods=['GET'])
def index():
    return render_template('/index.html', sectors=sectors, sectors_pred=sectors_french)


@app.route('/rank', methods=['GET'])
def rank():
    return render_template('/rank.html')


@app.route('/api/predict', methods=['POST'])
def predict():
    # select the model
    model = ""
    for item in models:
        if item["secteur"] == request.json['secteur']:
            model = item
            break

    if model == "":
        return jsonify({"message": "une erreur est survenue verifier le secteur!"}), 422

    try:
        y = model["predictions"].index.strftime("%m/%Y").values
        x = model["predictions"].values

        fig = get_line_plot(data_y=y, data_x=x, title=sectors_french[request.json['secteur']])

        return jsonify({
            "secteur": request.json['secteur'],
            "figure": f"{fig}",
            "x": dict(enumerate(x.flatten(), 1)),
            "y": dict(enumerate(y.flatten(), 1))
        }), 200

    except Exception as e:
        return jsonify({"message": "une erreur est survenue!"}), 500
