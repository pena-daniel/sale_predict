## Objectif du projet

L'objectif de ce projet est de prédires les différents prix de chaque type d'hébergement touristique.Ls prix que nous allons prédire sont: 
-   Income per stay unit nights sold (Revenu moyen par unité de séjour nuitée vendue)
-   Income from accommodation (Revenu de l'hébergement) 
-   Income from restaurant and bar sales (Revenus provenant des ventes de restaurants et de bars) 
-   Other income
-   Total income( Revenu total) 


## Description du Dataset

P6410-Tourism accomodation est une publication qui présente les résultats  issus de l'enquête mensuelle sur l'industrie de l'hébergement touristique , publié par le département de Statisque d'Afrique du Sud.

L'enquête sur l'hébergement touristique est une enquête mensuelle couvrant un échantillon d'entreprises publiques et privées impliquées dans l'industrie de l'hébergement de courte durée en Afrique du Sud. Les résultats de l'enquête sont utilisés pour compiler les estimations des comptes satellites du tourisme (CST) et du produit intérieur brut (PIB) et de ses composantes, qui pour l'élaboration et le suivi de la politique gouvernementale. Ces statistiques sont également utilisées dans l'analyse des performances comparatives des entreprises et des secteurs d'activité.


Cette enquête couvre les entreprises privées et publiques suivantes, déclarées aux impôts, dont l'activité principale est l'hébergement commercial de courte durée :
-   Hôtels (Hotels)
-   Parcs de caravanes et campings (Caravan parks and camping sites)
-    Guest houses and guest farms( Maisons d'hôtes et fermes d'hôtes)
-   Other accommodation (Autres hébergements):  Comprend les gîtes, les chambres d'hôtes, les gîtes ruraux et les "autres" établissements non classés ailleurs. établissements non classés ailleurs.

Pour chaque types d'hebergement, les variables observées sont les suivantes: 

-   Income per stay unit nights sold (Revenu moyen par unité de séjour nuitée vendue) :  Le tarif moyen par unité de séjour (c'est-à-dire le tarif par chambre dans un hôtel ou par emplacement motorisé dans un parc de caravanes) est calculé en divisant le revenu total de l'hébergement par le nombre de nuits vendues par unité de séjour vendues au cours de la période d'enquête.

-   Income from accommodation (Revenu de l'hébergement) : Revenus provenant des montants facturés pour les unités de séjour. Les "autres" revenus sont exclus (par exemple, les revenus provenant des repas).

-   Income from restaurant and bar sales (Revenus provenant des ventes de restaurants et de bars) : Revenus provenant des repas, des banquets et des boissons, ainsi que des ventes de tabac.

-   Other income (Autres revenus) : Revenus des jeux de casino, des services de blanchisserie et de téléphonie, des locations et redevances perçues pour les services de transport, les bureaux, les magasins, les garages, etc.

-   Occupancy rate(Taux d'occupation) :  Nombre de nuitées vendues divisé par le produit du nombre d'unités de séjour disponibles et du nombre de jours de la période d'enquête, exprimé en pourcentage.

-    Stay units available (Unité de séjour ): Unité d'hébergement disponible pour être facturée aux clients, par exemple un emplacement motorisé dans un parc de caravanes ou une chambre dans un hôtel.

-   Stay unit nights sold (Nuits vendues en unité de séjour) : Le nombre total d'unités de séjour occupées chaque nuit pendant la période d'enquête.

-   Total income( Revenu total) :  Comprend les revenus de l'hébergement, les revenus des ventes des restaurants et des bars et les "autres" revenus.

-   Income per stay unit nights sold(Revenu par unité de séjour nuitées vendues)
-   Income from accommodation( Revenus de l'hébergement)
-   Income from restaurant and bar sales( Recettes des restaurants et des bars)

Les colones H01 contient le numéro d'identitification du types de publication et HO2 contient le titre de la publication.
la colones H04 cotient les noms des variables observées
La colone H03 contient l'identifiant de chaque variable H03

La colones H16 permet de diffentier les variables avec les obsrvations reelles et les variables dont la saisonalité a été ajustée

La colonne H25 contient la périodicité (mensuelle) des observations 


## Models utilisés
3 models sont disponibles et le dernier c'est une library qui nous a permit de tester a la voler de nombreux models et d'en choisir le meilleur.
- ARIMA  https://www.jedha.co/formation-ia/algorithme-arima
- LSTM https://machinelearningmastery.com/how-to-develop-lstm-models-for-time-series-forecasting/
- ANN https://www.sciencedirect.com/science/article/pii/S2314728817300715
- PYCARET  https://pycaret.org/install/

## Lien pour tester

https://www.projet-python.penadaniel.com/myapp
